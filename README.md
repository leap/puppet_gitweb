# see also https://github.com/adrienthebo/puppet-gitweb.git
# puppet-gitweb #

Installs and configures gitweb

    class { "gitweb::settings":
      site_alias => "gitweb.domain.tld",
      projectroot => "/var/lib/gitolite/repositories",
    }

# magic
