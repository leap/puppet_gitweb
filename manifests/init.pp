# Class: gitweb
#   Installs gitweb and configures apache to serve it.
# Parameters:
#
# Actions:
#
# Requires:
#   - apache
#   - gitweb::settings
# Sample Usage:
#   include gitweb
class gitweb(
  $site_alias = 'leap.se',
  $documentroot = '/var/lib/gitolite/repositories/',
  $project_root = '/var/lib/gitolite/repositories/',
  $projects_list = '$project_root',
  $ssl = true) {
  package { 'gitweb':
    ensure => installed,
  }

  file { "/etc/gitweb.conf":
    ensure  => present,
    content => template('gitweb/gitweb.conf.erb'),
  }

  # if you want to enable push access through https
  # and 
  # include apache::mod::suexec
  #include apache::mod::rewrite
  #apache::vhost { $site_alias:
  #  priority => "10",
  #  ssl      => $ssl,
  #  docroot  => $doc_root,
  #  template => "gitweb/apache-gitweb.conf.erb",
  #  require  => [
  #    Class['apache::mod::rewrite'],
 #     Class['apache::mod::suexec'],
  #  ],
  #}
}
